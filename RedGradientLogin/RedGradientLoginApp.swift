//
//  RedGradientLoginApp.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 06/01/24.
//

import SwiftUI

@main
struct RedGradientLoginApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

struct NavigationBarBackButton: View {
    @Environment(\.presentationMode)
    var presentationMode
    
    var body: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack {
                Image(systemName: "arrow.left.circle.fill")
            }
            .foregroundColor(.white)
        }
    }
}
