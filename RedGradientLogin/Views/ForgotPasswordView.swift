//
//  ForgotPasswordView.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 07/01/24.
//

import SwiftUI

struct ForgotPasswordView: View {
    @State private var emailText: String = ""
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                VStack {
                    ZStack(alignment: .top) {
                        let gradient = Gradient(
                            colors: [Color(hex: "#FF0065"), Color(hex: "#FF4F63")]
                        )
                        LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
                        
                        Text("Forgot Password")
                            .multilineTextAlignment(.center)
                            .font(FontManager.headerMediumFont)
                            .padding(.top, 90)
                            .colorInvert()
                        
                        Text("New Password")
                            .font(FontManager.subTitleMediumFont)
                            .padding(.top, 160)
                            .colorInvert()
                    }
                    .edgesIgnoringSafeArea(.top)
                    .frame(height: 160)
                    
                    VStack(alignment: .leading, spacing: 16) {
                        Text("PLEASE ENTER YOUR EMAIL")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "email",
                            text: emailText,
                            type: .rounded
                        )
                        .autocapitalization(.none)
                        .padding(.horizontal)
                        .padding(.bottom)
                        
                        StyledButtonFactory(factory: ButtonConcreteFactory(), text: "send", type: .primary) {
                            print("clicou")
                        }
                        .padding(.horizontal)
                        .padding(.top, 10)
                    }
                    .padding(.top, 30)
                    .padding()
                }
            }
            .navigationBarItems(leading: NavigationBarBackButton())
        }
        .navigationBarBackButtonHidden(true)
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
