//
//  ContentView.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 06/01/24.
//

import SwiftUI
import CoreData

struct ContentView: View {

    var body: some View {
        LoginView()
            .navigationBarBackButtonHidden(true)
    }
}

#Preview {
    ContentView()
}
