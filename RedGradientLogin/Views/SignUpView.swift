//
//  SignUpView.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 12/01/24.
//

import SwiftUI

struct SignUpView: View {
    @State private var name: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var birthDate: String = ""
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                VStack {
                    ZStack(alignment: .top) {
                        let gradient = Gradient(
                            colors: [Color(hex: "#FF0065"), Color(hex: "#FF4F63")]
                        )
                        LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
                        
                        Text("Create new Account")
                            .multilineTextAlignment(.center)
                            .font(FontManager.headerMediumFont)
                            .padding(.top, 90)
                            .colorInvert()
                        
                        Text("Already Registred? Login")
                            .font(FontManager.subTitleMediumFont)
                            .padding(.top, 160)
                            .colorInvert()
                    }
                    .edgesIgnoringSafeArea(.top)
                    .frame(height: 160)
                    
                    VStack(alignment: .leading, spacing: 16) {
                        Text("PLEASE ENTER YOUR NAME")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                            .offset(y: 8)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "name",
                            text: name,
                            type: .rounded
                        )
                        .autocapitalization(.none)
                        .padding(.horizontal)
                        
                        Text("PLEASE ENTER YOUR EMAIL")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                            .offset(y: 8)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "email",
                            text: email,
                            type: .rounded
                        )
                        .autocapitalization(.none)
                        .padding(.horizontal)
                        
                        Text("PLEASE ENTER YOUR PASSWORD")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                            .offset(y: 8)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "password",
                            text: password,
                            type: .secureField
                        )
                        .padding(.horizontal)
                        
                        Text("PLEASE ENTER YOUR DATE OF BIRTH")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                            .offset(y: 8)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "date of birth",
                            text: birthDate,
                            type: .rounded
                        )
                        .autocapitalization(.none)
                        .padding(.horizontal)
                        
                        StyledButtonFactory(factory: ButtonConcreteFactory(), text: "sign up", type: .primary) {
                            print("sign up")
                        }
                        .padding(.horizontal)
                        .padding(.top, 30)
                    }
                    .padding(.top, 16)
                    .padding()
                }
            }
            .navigationBarItems(leading: NavigationBarBackButton())
        }
        .navigationBarBackButtonHidden(true)
    }
}

#Preview {
    SignUpView()
}
