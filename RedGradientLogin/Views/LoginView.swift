//
//  LoginView.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 06/01/24.
//

import SwiftUI

struct LoginView: View {
    @State private var emailText: String = ""
    @State private var passwordText: String = ""
    @State private var isSignUpButtonActive = false
    
    var body: some View {
        NavigationStack {
            GeometryReader { geometry in
                VStack {
                    ZStack(alignment: .top) {
                        let gradient = Gradient(
                            colors: [Color(hex: "#FF0065"), Color(hex: "#FF4F63")]
                        )
                        LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
                        
                        Text("Login")
                            .multilineTextAlignment(.center)
                            .font(FontManager.headerMediumFont)
                            .padding(.top, 90)
                            .colorInvert()
                        
                        Text("Sign in to continue")
                            .font(FontManager.subTitleMediumFont)
                            .padding(.top, 160)
                            .colorInvert()
                    }
                    .edgesIgnoringSafeArea(.top)
                    .frame(height: 180)
                    
                    VStack(alignment: .leading, spacing: 16) {
                        
                        Text("PLEASE ENTER YOUR EMAIL")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                            .offset(y: 8)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "email",
                            text: emailText,
                            type: .rounded
                        )
                        .autocapitalization(.none)
                        .padding(.horizontal)
                        
                        Text("PLEASE ENTER YOUR PASSWORD")
                            .kerning(1.5)
                            .padding(.leading)
                            .foregroundColor(Color(hex: "#8F8E8E"))
                            .font(FontManager.captionRegular)
                            .padding(.horizontal)
                            .offset(y: 8)
                        
                        StyledTextFieldFactory(
                            factory: TextFieldsStyleFactory(),
                            placeholder: "password",
                            text: passwordText,
                            type: .secureField
                        )
                        .padding(.horizontal)
                        
                        HStack {
                            Spacer()
                            NavigationLink("Forgot password", destination: ForgotPasswordView())
                                .font(FontManager.bodyRegular)
                                .foregroundColor(Color(hex: "#FF0065"))
                                .padding(.top,1)
                                .padding(.horizontal, 25)
                        }
                        
                            StyledButtonFactory(factory: ButtonConcreteFactory(), text: "login", type: .primary) {
                                print("login")
                            }
                            .padding(.horizontal)
                            .padding(.top, 30)
                       
                            StyledButtonFactory(factory: ButtonConcreteFactory(), text: "sign up", type: .secundary) {
                                isSignUpButtonActive = true
                            }
                            .navigationDestination(isPresented: $isSignUpButtonActive) {
                                SignUpView()
                            }
                            .padding(.horizontal)
                    }
                    .padding(.top, 30)
                    .padding()
                }
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
