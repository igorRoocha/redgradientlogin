//
//  FontManager.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 06/01/24.
//

import Foundation
import UIKit
import SwiftUI

struct FontManager {
    static let headerMediumFont = Font.custom(DMSans.mediumPt.rawValue, size: 36)
    static let subTitleMediumFont = Font.custom(DMSans.mediumPt.rawValue, size: 18)
    static let bodyRegular = Font.custom(DMSans.regularPt.rawValue, size: 16)
    static let bodySemiBold = Font.custom(DMSans.semiBoldPt.rawValue, size: 16)
    static let bodyBold = Font.custom(DMSans.boldPt.rawValue, size: 16)
    static let captionRegular = Font.custom(DMSans.regularPt.rawValue, size: 12)
    
    //MARK: Name of font family
    enum DMSans: String {
        case regularPt = "DMSanspt-Regular"
        case italicPt = "DMSanspt-Italic"
        case thinPt = "DMSanspt-Thin"
        case extraLightPt = "DMSanspt-ExtraLight"
        case thinItalictPt = "DMSanspt-ThinItalic"
        case extraLightItalicPt = "DMSanspt-ExtraLightItalic"
        case lightPt = "DMSanspt-Light"
        case lightItalicPt = "DMSanspt-LightItalic"
        case mediumPt = "DMSanspt-Medium"
        case mediumItalicPt = "DMSanspt-MediumItalic"
        case semiBoldPt = "DMSanspt-SemiBold"
        case semiBoldItalicPt = "DMSanspt-SemiBoldItalic"
        case boldPt = "DMSanspt-Bold"
        case boldItalicPt = "DMSanspt-BoldItalic"
        case extraBoldPt = "DMSanspt-ExtraBold"
        case extraBoldItalicPt = "DMSanspt-ExtraBoldItalic"
        case blackItalicPt = "DMSanspt-BlackItalic"
    }
    
    static func getCustomFontNames() {
        // get each of the font families
        for family in UIFont.familyNames.sorted() {
            let names = UIFont.fontNames(forFamilyName: family)
            // print array of names
            print("Family: \(family) Font names: \(names)")
        }
    }
}
