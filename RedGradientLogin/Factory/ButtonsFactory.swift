//
//  AbstractProtocols.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 07/01/24.
//

import SwiftUI

enum ButtonType {
    case primary
    case secundary
}

/// Protocol for the abstract factory
protocol ButtonAbstractFactory {
    associatedtype PrimaryButtonStyle: ButtonStyle
    associatedtype SecundaryButtonStyle: ButtonStyle
    
    func createPrimaryButtonStyle() -> PrimaryButtonStyle
    func createSecundaryButtonStyle() -> SecundaryButtonStyle
}

/// Concret implementation of factory to create buttons styles
struct ButtonConcreteFactory: ButtonAbstractFactory {
    func createPrimaryButtonStyle() -> PrimaryButtonStyle {
        return PrimaryButtonStyle()
    }
    
    func createSecundaryButtonStyle() -> SecundaryButtonStyle {
        return SecundaryButtonStyle()
    }
}

/// Primary Button style
struct PrimaryButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        let gradient = Gradient(
            colors: [Color(hex: "#FF0065"), Color(hex: "#FF4F63")]
        )
        
        configuration.label
            .foregroundColor(.white)
            .padding()
            .frame(width: 240, height: 50)
            .font(FontManager.bodyBold)
            .background(
                LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
            )
            .cornerRadius(25)
            .frame(width: 320, alignment: .center)
            .scaleEffect(configuration.isPressed ? 0.9 : 1.0)
    }
}

/// Secundary Button style
struct SecundaryButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        let rectangle = RoundedRectangle(cornerRadius: 25)
            .stroke(Color(hex: "#FF0065"))
            .background(.clear)
        
        configuration.label
            .foregroundColor(Color(hex: "#FF0065"))
            .padding()
            .frame(width: 240, height: 50)
            .font(FontManager.bodyBold)
            .background(rectangle)
            .cornerRadius(25)
            .frame(width: 320, alignment: .center)
            .scaleEffect(configuration.isPressed ? 0.9 : 1.0)
    }
}


/// Button styling factory
struct StyledButtonFactory<Factory: ButtonAbstractFactory>: View {
    let factory: Factory
    let text: String
    let type: ButtonType
    let action: () -> Void
    
    var body: some View {
        switch type {
        case .primary:
            Button(action: action) {
                Text(text)
            }
            .buttonStyle(factory.createPrimaryButtonStyle())
        case .secundary:
            Button(action: action) {
                Text(text)
            }
            .buttonStyle(factory.createSecundaryButtonStyle())
        }
    }
}
