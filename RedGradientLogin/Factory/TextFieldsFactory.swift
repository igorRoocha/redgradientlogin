//
//  TextFieldFactory.swift
//  RedGradientLogin
//
//  Created by Igor Rafael Rocha Klinger on 07/01/24.
//

import SwiftUI

enum TextFieldType {
    case rounded
    case secureField
}

/// Protocol for the abstract factory
protocol TextFieldStyleFactory {
    associatedtype CustomTextFieldStyle: TextFieldStyle
    func createTextFieldStyle(for type: TextFieldType) -> CustomTextFieldStyle
}

/// Concret implementation of factory to create text fields styles
struct TextFieldsStyleFactory: TextFieldStyleFactory {
    func createTextFieldStyle(for type: TextFieldType) -> some TextFieldStyle {
        switch type {
        case .rounded, .secureField: return RoundedTextFieldStyle()
        }
    }
}

/// Rounded text field style
struct RoundedTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        let roundedRectangle = RoundedRectangle(cornerRadius: 20)
            .fill(Color(hex: "#ECECEC"))
        
        configuration
            .padding()
            .background(roundedRectangle)
            .font(FontManager.bodyRegular)
            .foregroundColor(Color(hex: "#4D4C4C"))
    }
}

/// Text field styling factory
struct StyledTextFieldFactory<Factory: TextFieldStyleFactory>: View {
    let factory: Factory
    let placeholder: String
    let text: String
    let type: TextFieldType
    
    var body: some View {
        switch type {
        case .rounded:
            TextField(placeholder, text: .constant(text))
                .textFieldStyle(factory.createTextFieldStyle(for: type))
        case .secureField:
            SecureField(placeholder, text: .constant(text))
                .textFieldStyle(factory.createTextFieldStyle(for: type))
        }
    }
}
